"""
VQI image format support class for reading and writing dense and sparse images,objects,tables
"""

from __future__ import division

from builtins import str
from builtins import range
from builtins import object

__author__    = 'Dmitry Fedorov <dima@viqi.org>'
__version__   = '2.0'
__copyright__ = 'ViQi Inc'

import os
import sys
import traceback
import posixpath
from datetime import datetime
from collections import OrderedDict
import numpy as np
import csv
import math
import tables
#import skimage
import skimage.io
import skimage.transform

import logging
log = logging.getLogger(__name__ if __name__ != "__main__" else __file__.replace('.py', ''))

#------------------------------------------------------------------------------
# utils
#------------------------------------------------------------------------------

def bboxes_intersect(bb1, bb2):
    intersecting = (bb1[0] < bb2[0]+bb2[2] and bb2[0] < bb1[0]+bb1[2] and bb1[1] < bb2[1]+bb2[3] and bb2[1] < bb1[1]+bb1[3])
    return intersecting

def draw_sub_region(image, sub, offset):
    o1 = np.maximum(offset, 0)
    o2 = np.abs(np.minimum(offset, 0))

    sz2 = np.array(sub.shape) - o2
    sz = np.array(image.shape) - o1
    sz = np.minimum(sz, sz2)

    # resize the block into output region
    #image[o1[0]:o1[0]+sz[0], o1[1]:o1[1]+sz[1]] = sub[o2[0]:o2[0]+sz[0], o2[1]:o2[1]+sz[1]]

    if len(image.shape) == 3:
        a = image[o1[0]:o1[0]+sz[0], o1[1]:o1[1]+sz[1],:]
        b = sub[o2[0]:o2[0]+sz[0], o2[1]:o2[1]+sz[1],:]
        a[a==0] = b[a==0]
        image[o1[0]:o1[0]+sz[0], o1[1]:o1[1]+sz[1],:] = a
    else:
        a = image[o1[0]:o1[0]+sz[0], o1[1]:o1[1]+sz[1]]
        b = sub[o2[0]:o2[0]+sz[0], o2[1]:o2[1]+sz[1]]
        a[a==0] = b[a==0]
        image[o1[0]:o1[0]+sz[0], o1[1]:o1[1]+sz[1]] = a

#------------------------------------------------------------------------------
# VQIFormatReader - reads block and item data from VQI file
#------------------------------------------------------------------------------

class VQIFormatReader(object):

    def __init__(self, filename, path='/'):
        self.cache = []
        self.item_ids = []
        self.item_bbox = []
        self.item_features = []
        self.atom = None
        self.filename = filename
        self.title = None
        self.h5file = None
        self.filters = None
        self.cached_items = 0
        self.block_id = 0
        self.blocked_store = True
        self.cache_size = 0
        self.space_dims = None
        self.path = path
        self.block_bbox = None
        self.bboxes = []
        self.block_ids = []
        self.id_offsets = []
        self.columns_reference = ['mask']
        self.column_id = 'id'
        self.column_label = 'class'
        self.image_content = None
        self.tile_size = None
        self.image_dtype = None
        self.table_name = 'table'
        self.feature_table_defs = None
        self.expected_elements = 0
        self.number_sparse_levels = 0
        self.chunkshape = None
        self.storage_version = __version__
        self.storage_type = None
        self.image_dimensions = 'X,Y,C'
        self.compression_library = 'zlib'
        self.compression_level = 0 # 0-no compression, 9-highest level, pretty slow
        #self.image_type = 'planar'

        # when initializing VQI reader, get metadata first
        if self.space_dims is None:
            self._init_reader()

    def __del__(self):
        self.close()

    def _init_reader(self):
        self.columns_info = self.read_columns_info()
        self.columns_reference = self.columns_info.get('column_ref', ['mask'])
        self.column_label = self.columns_info.get('column_label', 'class')

        self.column_id = self.columns_info.get('column_id', 'object_id')
        self.space_dims = self.columns_info.get('space_dims', None)
        self.image_dtype = self.columns_info.get('image_dtype', None) # dima: we need to find this from blocks stored in the image

    #------------------------------------------------------------------------------
    # exposed methods for storing individual objects
    #------------------------------------------------------------------------------

    def close(self):
        self.finish()
        self._finish_batch()

    def start(self, block_bbox=None, block_id=None):
        '''
        block_bbox can be automatically computed from element's positions
        use these options to provide a specific grouping of elements, for example into FOVs
        '''
        pass

    def finish(self):
        if len(self.cache)==0:
            return

    #------------------------------------------------------------------------------
    # reading
    #------------------------------------------------------------------------------

    def read_columns_info(self):
        '''
        reads information about table
        '''
        log.debug('Reading columns used for ML, file: %s', self.filename)
        self._start_batch(mode='r')
        try:
            return self._get_column_info()
        except Exception:
            log.exception('Error fetching column info')
            return {}
        finally:
            self._finish_batch()

    def read_class_stats(self):
        '''
        reads counts of objects per class
        '''
        log.debug('read_class_stats, file: %s', self.filename)
        self._start_batch(mode='r')
        try:
            info = self._get_column_info()
            labels = info['labels_mapping'].keys()
            label_col = info['column_label']
            class_counts = dict((l, 0) for l in labels)
            table = self.h5file.get_node(self.path, 'table')
            condvars = {'label_col': table.colinstances[label_col]}
            for l in labels:
                query = '(label_col==%s)'%(l)
                r = table.read_where(query, condvars=condvars)
                class_counts[l] = r.shape[0]
            return class_counts
        except Exception:
            log.exception('Error while reading class counts')
            return None
        finally:
            self._finish_batch()

    def read_table(self, query=None):
        '''
        read table in an array format
        note: can use _rows_to_dicts to convert into list of dicts if needed
        '''
        log.debug('read_table, query: %s, file: %s', query, self.filename)
        self._start_batch(mode='r')
        try:
            # read with query
            table = self.h5file.get_node(self.path, 'table')
            if query is None:
                return table.read()
            else:
                return table.read_where(query)
        except Exception:
            log.exception('Error in read_table')
            return None
        finally:
            self._finish_batch()

    def _rows_to_dicts(self, rows):
        '''
        convert array output to a list of dicts
        '''
        o = []
        d = rows.dtype
        for r in rows:
            o.append( dict([(d.names[i], v) for i,v in enumerate(r)]) )
        if rows.shape[0]==1: return o[0]
        return o

    def read_item(self, item_id, level=0):
        '''
        reads object's image, features and bounding box returning a dict
        '''
        log.debug('read_object: %s, file: %s', item_id, self.filename)
        self._start_batch(mode='r')
        try:
            # read item image
            path = posixpath.join(self.path, 'level_{0:03}'.format(level))
            item_id_offsets = self.h5file.get_node_attr(path, 'viqi_block_item_id_offsets')
            block_id = item_id_offsets.shape[0]-1
            for i in range(block_id, -1, -1):
                if item_id >= item_id_offsets[i]:
                    block_id = i
                    break

            item_image, bbox, block_bbox = self._fetch_item_image(block_id, item_id, level=level)

            query='(%s==%s)'%(self.column_id, item_id)
            table = self.h5file.get_node(self.path, 'table')
            rows = table.read_where(query)

            # convert row to dict
            features = self._rows_to_dicts(rows)
            features['_idx'] = item_id
            features['_image'] = item_image
            features[self.columns_reference] = item_image
            features['_bbox'] = bbox
            features['_block_bbox'] = block_bbox
            return features
        except Exception:
            log.exception('Error in read_item')
            return {}
        finally:
            self._finish_batch()

    def read_block_items(self, block_id, level=0, block_column=None):
        '''
        reads all objects in a block returning a lists of dictionaries containing:
        images, features and bounding boxes
        system defined: _idx, _bbox, _block_bbox, _image
        '''
        log.debug('block_id: %s, file: %s', block_id, self.filename)
        self._start_batch(mode='r')
        try:
            images, idxs, bboxs, block_bbox = self._fetch_block_items(block_id, level=level)

            if block_column is not None:
                query = '(%s==%s)'%(block_column, block_id)
            else:
                query = '({0}>={1}) & ({0}<={2})'.format(self.column_id, idxs[0], idxs[-1])

            # read and convert features to list of dicts
            log.debug('Query: "%s"', query)
            table = self.h5file.get_node(self.path, 'table')
            rows = table.read_where(query)
            features = self._rows_to_dicts(rows)

            for i, f in enumerate(features):
                f['_idx'] = idxs[i]
                f['_image'] = images[i]
                f[self.columns_reference] = images[i]
                f['_bbox'] = bboxs[i]
                f['_block_bbox'] = block_bbox

            return features
        except Exception:
            log.exception('Error in read_block_items')
            return []
        finally:
            self._finish_batch()

    def read_image_region(self, level, bbox):
        '''
        reads an arbitrary region from the whole image
        '''
        log.debug('read_image_region, file: %s', self.filename)
        self._start_batch()

        node_level = self.h5file.get_node(self.path, 'level_{0:03}'.format(level))
        block_bboxes = node_level._v_attrs['viqi_storage_block_bboxes']
        image = self._fetch_dense_region(level, bbox, block_bboxes)

        self._finish_batch()
        return image

    def read_pixel_values(self, level, pts):
        '''
        pts is a list of tuples containing i,j,[z,[t,[...]]]
        '''
        log.debug('read_pixel_values, file: %s', self.filename)
        self._start_batch()
        pixels = self._fetch_pixel_values(level, pts)
        self._finish_batch()
        return pixels

    #------------------------------------------------------------------------------
    # internal methods
    #------------------------------------------------------------------------------

    def _start_batch(self, mode='a'):
        if self.h5file is None:
            log.debug('Opening HDF-5 store: %s', self.filename)
            self.filters = tables.Filters(complevel=self.compression_level, complib=self.compression_library)
            self.h5file = tables.open_file(self.filename, mode=mode, title=self.title, filters=self.filters)

    def _finish_batch(self):
        if self.h5file is not None:
            self.h5file.close()
            self.h5file = None
            log.debug('Closed HDF-5 store: %s', self.filename)

    def _get_column_info(self):
        ml = self._safe_attribute_vector(self.path, 'viqi_columns_ml')
        if ml is not None: ml = [i == 'true' for i in ml]

        units = self._safe_attribute_vector(self.path, 'viqi_units')

        try:
            table = self.h5file.get_node(self.path, 'table')
            names = table.colnames
            coltypes = table.coltypes
            names_ml_inc = [names[i] for i in range(len(ml)) if ml[i] is True]
            names_ml_exc = [names[i] for i in range(len(ml)) if ml[i] is False]
        except Exception:
            names = None
            coltypes = None
            names_ml_inc = None
            names_ml_exc = None
            log.exception('Could not get column names')

        labels_s = self._safe_attribute_vector(self.path, 'viqi_class_label_mapping')
        try:
            labels = dict([(int(i.split(':')[0]), i.split(':')[1]) for i in labels_s])
        except Exception:
            labels = {}

        return {
            'labels_mapping': labels,
            'ml': ml,
            'units': units,
            'columns': names,
            'column_types': coltypes,
            'columns_ml_excluded': names_ml_exc,
            'columns_ml_included': names_ml_inc,
            'column_label': self._safe_attribute_vector_first(self.path, 'viqi_columns_class'),
            'column_conf': self._safe_attribute_vector_first(self.path, 'viqi_columns_confidence'),
            'column_id': self._safe_attribute_vector_first(self.path, 'viqi_columns_id'),
            'column_ref': self._safe_attribute_vector_first(self.path, 'viqi_columns_reference'),
            'space_dims': self._safe_attribute(self.path, 'viqi_image_size'),
            'image_dtype': np.uint8, # dima: we need to find this from blocks stored in the image
        }

    def _get_string_attribute(self, node, name):
        try:
            v = node._v_attrs[name].decode('utf-8')
        except Exception:
            v = node._v_attrs[name]
        return v

    def _safe_attribute(self, path, name, default=None):
        try:
            v = self.h5file.get_node_attr(path, name)
        except AttributeError:
            v = default
        try:
            v = v.decode('utf-8')
        except Exception:
            pass
        return v

    def _safe_attribute_vector(self, path, name, default=None):
        try:
            v = self._safe_attribute(path, name)
            return v.split(',')
        except Exception:
            v = default
        return v

    def _safe_attribute_vector_first(self, path, name, default=None):
        try:
            v = self._safe_attribute(path, name)
            v = v.split(',')
            return v[0]
        except Exception:
            v = default
        return v

    #------------------------------------------------------------------------------
    # reading elements and dense blocks
    #------------------------------------------------------------------------------

    def _fetch_item_image(self, block_id, item_id, level=0):
        '''
        Outputs:
            image - image np.array
            bbox - bounding box within a block
        '''
        path = posixpath.join(self.path, 'level_{0:03}'.format(level))
        block_name = 'block_{0:04}'.format(block_id)
        block_path = posixpath.join(path, block_name)

        # sparse block may not be present if there were no elements in it
        try:
            node = self.h5file.get_node(path, block_name)
        except Exception:
            log.exception('Error in _fetch_item_image, could not open node %s', block_name)
            return (None, None, None)

        if self._get_string_attribute(node, 'viqi_block_format') != 'sparse':
            return (None, None, None)

        block_bbox = node._v_attrs['viqi_block_bbox']
        mult = 1 if node.dtype != np.bool else 255
        page = node.read()
        index_name = '{0}_sparse_index'.format(block_name)
        node_index = self.h5file.get_node(path, index_name)
        page_index = node_index.read()

        N = page_index.shape[0]
        for i in range(N):
            idx = page_index[i, 0]
            if idx == item_id:
                pos = page_index[i, 1]
                x = page_index[i, 2]
                y = page_index[i, 3]
                w = page_index[i, 4]
                h = page_index[i, 5]
                item_image = page[pos:pos+w,0:h] * mult
                return (item_image, (x,y,w,h), block_bbox)

        return (None, None, None)

    def _fetch_block_items(self, block_id, level=0):
        '''
        Outputs:
            images - list of image np.arrays
            idxs - list of item ids
            bboxs - list of bounding boxes within a block
        '''
        path = posixpath.join(self.path, 'level_{0:03}'.format(level))
        block_name = 'block_{0:04}'.format(block_id)
        block_path = posixpath.join(path, block_name)

        # sparse block may not be present if there were no elements in it
        try:
            node = self.h5file.get_node(path, block_name)
        except Exception:
            log.exception('Error in _fetch_block_items, could not open node %s', block_name)
            return ([], [], [], ())

        if self._get_string_attribute(node, 'viqi_block_format') != 'sparse':
            return ([], [], [], ())

        block_bbox = node._v_attrs['viqi_block_bbox']

        mult = 1 if node.dtype != np.bool else 255
        page = node.read()
        index_name = '{0}_sparse_index'.format(block_name)
        node_index = self.h5file.get_node(path, index_name)
        page_index = node_index.read()

        images = []
        bboxs = []
        idxs = []
        N = page_index.shape[0]
        for i in range(N):
            idx = page_index[i, 0]
            pos = page_index[i, 1]
            x = page_index[i, 2]
            y = page_index[i, 3]
            w = page_index[i, 4]
            h = page_index[i, 5]
            item_image = page[pos:pos+w,0:h] * mult

            images.append(item_image)
            bboxs.append((x,y,w,h))
            idxs.append(idx)

        return (images, idxs, bboxs, block_bbox)

    def _fetch_dense_block(self, node):
        return node.read()

    def _fetch_sparse_block(self, node, path, block_name, bbox=None):
        if bbox is None:
            bbox = node._v_attrs['viqi_block_bbox']
        block_image = np.zeros((bbox[2],bbox[3]), self.image_dtype)

        mult = 1 if node.dtype != np.bool else 255
        page = node.read()
        index_name = '{0}_sparse_index'.format(block_name)
        node_index = self.h5file.get_node(path, index_name)
        page_index = node_index.read()
        N = page_index.shape[0]
        for i in range(N):
            idx = page_index[i, 0]
            pos = page_index[i, 1]
            x = page_index[i, 2]
            y = page_index[i, 3]
            w = page_index[i, 4]
            h = page_index[i, 5]
            try:
                block_image[x:x+w,y:y+h] = page[pos:pos+w,0:h] * mult
                #a = block_image[x:x+w,y:y+h]
                #b = page[pos:pos+w,0:h] * mult
                #a[a==0] = b[a==0]
            except ValueError:
                sz = block_image[x:x+w,y:y+h].shape # errors in interpolation may cause broadcast errors
                w = sz[0]
                h = sz[1]
                block_image[x:x+w,y:y+h] = page[pos:pos+w,0:h] * mult
                #a = block_image[x:x+w,y:y+h]
                #b = page[pos:pos+w,0:h] * mult
                #a[a==0] = b[a==0]

        return block_image

    def _fetch_block(self, level, block_id, bbox=None):
        path = posixpath.join(self.path, 'level_{0:03}'.format(level))
        block_name = 'block_{0:04}'.format(block_id)
        block_path = posixpath.join(path, block_name)

        # sparse block may not be present if there were no elements in it
        try:
            node = self.h5file.get_node(path, block_name)
        except Exception:
            if bbox is None:
                raise ModuleException('Bbox is required for empty sparse blocks')
            return np.zeros((bbox[2],bbox[3]), self.image_dtype)

        if self._get_string_attribute(node, 'viqi_block_format') == 'sparse':
            return self._fetch_sparse_block(node, path, block_name, bbox=bbox)
        return self._fetch_dense_block(node)

    def _fetch_dense_region(self, level, bbox, block_bboxes):
        log.debug('_fetch_dense_region at level: %s, bbox: %s', level, str(bbox))
        region = None
        N = block_bboxes.shape[0]
        for i in range(N):
            bbox2 = block_bboxes[i,:]
            is_intersecting = bboxes_intersect(bbox, bbox2)
            if is_intersecting == True:
                #log.debug('%s integrating %s', str(bbox), i)
                block_image = self._fetch_block(level, i, bbox=bbox2)
                #log.debug('Block image shape: %s', block_image.shape)
                if region is None:
                    self.image_dtype = block_image.dtype
                    if len(block_image.shape) == 3:
                        region = np.zeros((bbox[2],bbox[3],block_image.shape[2]), self.image_dtype)
                    else:
                        region = np.zeros((bbox[2],bbox[3]), self.image_dtype)
                offset = [0]*len(block_image.shape)
                offset[0] = bbox2[0]-bbox[0]
                offset[1] = bbox2[1]-bbox[1]
                #log.debug('offset: %s', offset)
                draw_sub_region(region, block_image, tuple(offset))

        return region

    def _fetch_block_pixels(self, level, block_id, pts):
        path = posixpath.join(self.path, 'level_{0:03}'.format(level))
        block_name = 'block_{0:04}'.format(block_id)
        block_path = posixpath.join(path, block_name)
        pixels = []
        # sparse block may not be present if there were no elements in it
        try:
            node = self.h5file.get_node(path, block_name)
            if self._get_string_attribute(node, 'viqi_block_format') == 'sparse':
                return pixels # dima: for now only support dense storage for pixels
            for p in pts:
                v = node[p[0]:p[0]+1,p[1]:p[1]+1,:]
                pixels.append(v.squeeze())
        except Exception:
            log.exception('Error in _fetch_block_pixels')
            return pixels
        return pixels

    def _fetch_pixel_values(self, level, pts):
        log.debug('_fetch_pixel_values at level: %s', level)
        pixels = {} # key at point containing pixel values
        node_level = self.h5file.get_node(self.path, 'level_{0:03}'.format(level))
        block_bboxes = node_level._v_attrs['viqi_storage_block_bboxes']

        for block_id,bbox in enumerate(block_bboxes):
            block_pts = []
            image_pts = []
            for p in pts:
                bbox2 = (p[0], p[1], 1, 1)
                is_intersecting = bboxes_intersect(bbox, bbox2)
                if is_intersecting == True:
                    block_pts.append((p[0]-bbox[0], p[1]-bbox[1]))
                    image_pts.append(p)
            block_pixels = self._fetch_block_pixels(level, block_id, block_pts)
            for i,p in enumerate(block_pixels):
                pixels[image_pts[i]] = p
        return pixels
