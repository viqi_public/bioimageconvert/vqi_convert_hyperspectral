"""
VQI image format support class for reading and writing dense and sparse images,objects,tables
"""

from __future__ import division

from builtins import str
from builtins import range
from builtins import object

__author__    = 'Dmitry Fedorov <dima@viqi.org>'
__version__   = '2.0'
__copyright__ = 'ViQi Inc'

import os
import sys
import traceback
import posixpath
from datetime import datetime
from collections import OrderedDict
import numpy as np
import csv
import math
import tables
#import skimage
import skimage.io
import skimage.transform

import logging
log = logging.getLogger(__name__ if __name__ != "__main__" else __file__.replace('.py', ''))

from vqi_format_reader import VQIFormatReader

#------------------------------------------------------------------------------
# misc
#------------------------------------------------------------------------------

MIN_SIZE_CARRAY = 5000
OBJECT_ID_BLOCK_MULTIPLIER = 1000000

SKIMAGE_OLD_VERSION = False
try:
    import skimage
    skiv = [int(i) for i in skimage.__version__.split('.')]
    if skiv[0]==0 and skiv[1]<14:
        SKIMAGE_OLD_VERSION = True
except Exception:
    SKIMAGE_OLD_VERSION = True

#------------------------------------------------------------------------------
# utils
#------------------------------------------------------------------------------

def ensure_even_size_with_padding(a):
    if (a % 2) == 0:
       return a + 2
    else:
       return a + 3

#------------------------------------------------------------------------------
# image interpolation
#------------------------------------------------------------------------------

def image_stats(img):
    minv = float(img.min())
    maxv = float(img.max())
    return {
        'dtype': img.dtype,
        'is_float': not np.issubdtype(img.dtype, np.integer),
        'min': minv,
        'max': maxv,
        'range': (maxv-minv)
    }

def image_normalize_01(img):
    '''
    returns an image rescaled into [0..1] in float64
    '''
    stats = image_stats(img)
    img = img.astype(np.float64)
    img -= stats['min']
    img /= stats['range']
    return img

def image_revert_range(img, stats, rescale=True):
    '''
    returns an image rescaled into original image statistics
    if rescale=True the data range is normalized to stats min and max otherwise normalization will not happen
    stats provides original image statistics for range and type as returned by image_stats
    '''
    img = np.copy(img)
    if rescale is True:
        img *= stats['range']
        img += stats['min']
    if stats['is_float'] is not True:
        img = np.round(img)
    np.clip(img, stats['min'], stats['max'], out=img)
    return img.astype(stats['dtype'])

def image_resize(img, new_shape, preserve_type=True, stats=None):
    '''
    returns an image resized to fit new_shape
    if preserve_type=True the output image will be of the same type and data range as input
    if preserve_type=False an float64 image is returned within the same range but with interpolated values in between
    stats can provide original image statistics for range and type as returned by image_stats
    '''
    if preserve_type is True and stats is None:
        stats = image_stats(img)

    if SKIMAGE_OLD_VERSION is True:
        img = skimage.transform.resize(img, new_shape, order=0, mode='reflect', clip=True, preserve_range=True)
    else:
        img = skimage.transform.resize(img, new_shape, order=0, mode='reflect', clip=True, preserve_range=True, anti_aliasing=True)

    if preserve_type is True and stats is not None:
        if stats['dtype'] == np.bool: # spacial processing for mask data
            img = img >= 0.5
        img = image_revert_range(img, stats, rescale=False)
    return img

def image_pyramid_reduce(img, preserve_type=True, stats=None):
    '''
    returns downscaled level 50% smaller than original image
    if preserve_type=True the output image will be of the same type and data range as input
    if preserve_type=False an float64 image is returned within the same range but with interpolated values in between
    stats can provide original image statistics for range and type as returned by image_stats
    '''
    if preserve_type is True and stats is None:
        stats = image_stats(img)

    log.debug('image_pyramid_reduce initial stats: %s', str(stats))

    # pyramid_reduce produces output ranged 0-1
    # if SKIMAGE_OLD_VERSION is True:
    #     img = skimage.transform.pyramid_reduce(img, downscale=2, order=0, mode='reflect')
    # else:
    #     img = skimage.transform.pyramid_reduce(img, downscale=2, order=0, mode='reflect', multichannel=False)
    # if stats is not None:
    #     img *= stats['range']
    # if preserve_type is True and stats is not None:
    #     img = image_revert_range(img, stats, rescale=False)

    # preferred method
    sf = [1]*len(img.shape)
    sf[0] = 2
    sf[1] = 2
    img = skimage.transform.downscale_local_mean(img, tuple(sf), clip=True) # this produces same range as input data for ints
    if preserve_type is True and stats is not None:
        if stats['dtype'] == np.bool: # spacial processing for mask data
            img = img >= 0.5
        img = image_revert_range(img, stats, rescale=False) # dima: any enhancement must be global, can't run local block enhancement!

    log.debug('image_pyramid_reduce final stats: %s', str(image_stats(img)))
    return img


#------------------------------------------------------------------------------
# VQIFormatWriter - writes block and item data to VQI file
#------------------------------------------------------------------------------

class VQIFormatWriter(VQIFormatReader):

    def __init__(self, filename, title='', path='/', space_dims=None, block_limit_elements=0, blocked_store=True,
                 feature_table_defs=None, expected_elements=3000000, column_id='id', column_ref='mask',
                 image_content='mask', number_sparse_levels=3, chunkshape=(256, 256),
                 storage_type='table,image,sparse_image', image_dimensions='X,Y,C',
                 compression_library='zlib', compression_level=9, tile_size=512):
        self.cache = []
        self.item_ids = []
        self.item_bbox = []
        self.item_features = []
        self.atom = None
        self.filename = filename
        self.title = title
        self.h5file = None
        self.filters = None
        self.cached_items = 0
        self.block_id = 0
        self.blocked_store = blocked_store
        self.cache_size = block_limit_elements
        self.space_dims = space_dims
        self.path = path
        self.block_bbox = None
        self.bboxes = []
        self.block_ids = []
        self.id_offsets = []
        self.columns_reference = [column_ref]
        self.column_id = column_id
        self.image_content = image_content
        self.tile_size = tile_size
        self.image_dtype = None
        self.table_name = 'table'
        self.feature_table_defs = feature_table_defs
        self.expected_elements = expected_elements
        self.number_sparse_levels = number_sparse_levels
        self.chunkshape = chunkshape
        self.storage_version = __version__
        self.storage_type = storage_type
        self.image_dimensions = image_dimensions
        self.compression_library = compression_library
        self.compression_level = compression_level # 0-no compression, 9-highest level, pretty slow
        #self.image_type = 'planar'

        # when initializing VQI reader, get metadata first
        if self.space_dims is None:
            self._init_reader()

    #------------------------------------------------------------------------------
    # exposed methods for storing individual objects
    #------------------------------------------------------------------------------

    def get_cached_items():
        return self.cached_items

    def close(self):
        self.finish()
        self._finish_batch()

    def start(self, block_bbox=None, block_id=None):
        '''
        block_bbox can be automatically computed from element's positions
        use these options to provide a specific grouping of elements, for example into FOVs
        '''
        self.block_bbox = block_bbox
        self.bboxes.append(self.block_bbox)
        if block_id is None:
            block_id = len(self.block_ids)
        self.block_ids.append(block_id)

    def finish(self):
        if len(self.cache)==0:
            return
        if self.cache_size==0 and self.blocked_store is True:
            self._store_block()
        elif self.cache_size==0 and self.blocked_store is False:
            self._store_cache()

    def add_to_cache(self, img, idx, bbox, features=None):
        #log.debug('Adding cache element to %s items, file: %s', len(self.cache), self.filename)
        if self.image_dtype is None:
            self.image_dtype = img.dtype

        if self.cache_size>0 and len(self.cache)>=self.cache_size:
            if self.blocked_store is True:
                self._store_block() # store combined matrices
            else:
                self._store_cache() # store individual matrices

        if idx is None:
            idx = self.compute_next_object_id()
            features = self.update_feature_indices(features, idx)
        self.cache.append(img)
        self.item_ids.append(idx)
        self.item_bbox.append(bbox)
        self.item_features.append(features)

        self.cached_items += 1

    def get_current_block_id(self):
        try:
            block_id = self.block_ids[-1]
        except IndexError:
            block_id = 0
        return block_id

    def get_current_block_bbox(self):
        try:
            bbox = self.bboxes[-1]
        except IndexError:
            bbox = (0,0,0,0)
        return bbox

    def compute_next_object_id(self, idx=None):
        block_id = self.get_current_block_id()
        if idx is None:
            try:
                idx = self.item_ids[-1] + 1
            except IndexError:
                idx = 0

        return (block_id * OBJECT_ID_BLOCK_MULTIPLIER) + idx

    def update_feature_indices(self, features, idx):
        if features is None:
            return None
        features[self.column_id] = idx
        for r in self.columns_reference:
            features[r] = idx
        return features

    #------------------------------------------------------------------------------
    # writing
    #------------------------------------------------------------------------------

    def update_label(self, label_num, label):
        '''
        update all objects to a new label
        '''
        log.debug('update_label: %s, file: %s', label_num, self.filename)
        self._start_batch(mode='a')
        try:
            labels_id = self._safe_attribute(self.path, 'viqi_columns_class', default='class')
            label_id = labels_id.split(',')[0]

            table = self.h5file.get_node(self.path, 'table')
            for row in table.iterrows():
                row[label_id] = label_num
                row.update()
            table.flush()

            # update class mapping
            label_mapping = '%s:%s'%(label_num, label)
            self.h5file.set_node_attr(self.path, 'viqi_class_label_mapping', label_mapping)
        except Exception:
            log.exception('Error in update_label')
            return {}
        finally:
            self._finish_batch()

    def set_metadata(self, key, val):
        '''
        update all objects to a new label
        '''
        log.debug('set_metadata: %s:%s, file: %s', key, val, self.filename)
        self._start_batch(mode='a')
        try:
            self.h5file.set_node_attr(self.path, key, val)
        except Exception:
            log.exception('Error in set_metadata')
            return {}
        finally:
            self._finish_batch()

    def write_dense_block(self, block_id, image, bbox, level=0, scale=1.0):
        '''
        write dense block into respected level
        '''
        log.debug('block_id: %s, file: %s', block_id, self.filename)
        self.blocked_store = True
        self.start(block_bbox=bbox, block_id=block_id) # necessary block-level adjustments
        if self.image_dtype is None:
            self.image_dtype = image.dtype

        self._start_batch(mode='a')
        try:
            self._store_dense_block(level, block_id, image, bbox)
            self._store_block_metadata(level=level, scale=scale)
        except Exception:
            log.exception('Error while writing dense block')
        finally:
            self._finish_batch()

    #------------------------------------------------------------------------------
    # internal methods
    #------------------------------------------------------------------------------

    def _advance_block(self):
        self.block_id += 1
        self.cache = []
        self.item_ids = []
        self.item_bbox = []
        self.item_features = []

    def _store_cache(self):
        if self.blocked_store is True:
            return
        if len(self.cache)<1:
            return

        log.debug('Storing dense %s items, file: %s', len(self.cache), self.filename)
        self._start_batch()
        self._append_cache_to_table()

        if self.h5file is None:
            raise ModuleException('Attempt to write into closed HDF5 store')

        for i, img in enumerate(self.cache):
            idx = self.item_ids[i]

            cell_dir = int(math.floor(idx / 10000.0))
            path = posixpath.join(self.path, '{0:08}'.format(cell_dir*10000))
            name = '{0:08}'.format(idx)
            atom = tables.Atom.from_dtype(img.dtype)

            # use compression for arrays of more than 5000 elements
            if np.prod(img.shape) > MIN_SIZE_CARRAY:
                self.h5file.create_carray(path, name, obj=img, createparents=True, filters=self.filters, shape=img.shape, atom=atom, chunkshape=img.shape)
            else:
                self.h5file.create_array(path, name, obj=img, createparents=True, shape=img.shape, atom=atom)

        self._advance_block()
        self._finish_batch()

    def _store_block_metadata(self, level=0, scale=1.0):
        path_level = posixpath.join(self.path, 'level_{0:03}'.format(level))

        if len(self.bboxes) == 1:
            # add tags describing blocked format if used, cells per block, etc
            self.h5file.set_node_attr('/', 'viqi_storage_version', self.storage_version)
            self.h5file.set_node_attr('/', 'viqi_storage_type', self.storage_type)
            if len(self.id_offsets)>0:
                self.h5file.set_node_attr(self.path, 'viqi_storage_subtype', 'combined')
                self.h5file.set_node_attr(self.path, 'viqi_columns_reference', ','.join(self.columns_reference))
                self.h5file.set_node_attr(self.path, 'viqi_columns_id', self.column_id)
            if self.space_dims is not None:
                self.h5file.set_node_attr(self.path, 'viqi_image_type', 'planar')
                self.h5file.set_node_attr(self.path, 'viqi_image_content', self.image_content)
                self.h5file.set_node_attr(self.path, 'viqi_image_dimensions', self.image_dimensions)
                self.h5file.set_node_attr(self.path, 'viqi_image_size', np.array(self.space_dims, dtype=np.uint32))
            #self.h5file.set_node_attr(self.path, 'viqi_table_type', 'dense')

        # store block bboxes index in the root
        BBOXS = np.zeros((len(self.bboxes), 4), np.uint32)
        for i, bbox in enumerate(self.bboxes):
            BBOXS[i, :] = bbox
        self.h5file.set_node_attr(path_level, 'viqi_storage_block_bboxes', BBOXS)

        # store resolution-level metadata
        self.h5file.set_node_attr(path_level, 'viqi_level_scale', scale)
        self.h5file.set_node_attr(path_level, 'viqi_storage_number_blocks', len(self.bboxes))
        if len(self.id_offsets)>0:
            self.h5file.set_node_attr(path_level, 'viqi_storage_sparse_items', self.cached_items)
            self.h5file.set_node_attr(path_level, 'viqi_block_item_id_offsets', np.array(self.id_offsets, dtype=np.uint32))

    def _store_block_pyramid(self, path, block_name, block_image, block_index):
        if self.number_sparse_levels <= 1:
            return
        stats = image_stats(block_image)
        level = 0
        #while block_image.shape[0]>8 and block_image.shape[1]>8:
        while level<self.number_sparse_levels-1:

            block_image = image_pyramid_reduce(block_image, preserve_type=True, stats=stats)

            level += 1
            level_path = posixpath.join(path, 'level_{0:03}'.format(level))
            block_path = posixpath.join(level_path, block_name)

            atom = tables.Atom.from_dtype(block_image.dtype)
            chunkshape = (256, block_image.shape[1])
            if np.prod(block_image.shape) > MIN_SIZE_CARRAY:
                self.h5file.create_carray(level_path, block_name, obj=block_image, createparents=True, filters=self.filters, shape=block_image.shape, atom=atom, chunkshape=chunkshape)
            else:
                self.h5file.create_array(level_path, block_name, obj=block_image, createparents=True, shape=block_image.shape, atom=atom)

            power_2_level = math.pow(2, level)*1.0
            IDX = np.copy(block_index)
            for i in range(IDX.shape[0]):
                s = math.pow(2, level)*1.0
                IDX[i, 1] = int(round(block_index[i, 1]/power_2_level))
                IDX[i, 2] = int(round(block_index[i, 2]/power_2_level))
                IDX[i, 3] = int(round(block_index[i, 3]/power_2_level))
                IDX[i, 4] = int(round(block_index[i, 4]/power_2_level))
                IDX[i, 5] = int(round(block_index[i, 5]/power_2_level))

            index_name = '{0}_sparse_index'.format(block_name)
            idx_atom = tables.Atom.from_dtype(IDX.dtype)
            chunkshape = (256, IDX.shape[1])
            self.h5file.create_carray(level_path, index_name, obj=IDX, createparents=True, filters=self.filters, shape=IDX.shape, atom=idx_atom, chunkshape=chunkshape)

            bbox = [int(round(v/power_2_level)) for v in self.block_bbox]
            self.h5file.set_node_attr(block_path, 'viqi_block_bbox', np.array(bbox, dtype=np.uint32)) # extent of the transformed block in the image space
            self.h5file.set_node_attr(block_path, 'viqi_block_format', 'sparse')
            self.h5file.set_node_attr(block_path, 'viqi_block_content', self.image_content)

            # level information
            #self._store_block_metadata(level=level, scale=1.0/power_2_level)
            self.h5file.set_node_attr(level_path, 'viqi_level_scale', 1.0/power_2_level)
            self.h5file.set_node_attr(level_path, 'viqi_storage_number_blocks', self.block_id+1)
            self.h5file.set_node_attr(level_path, 'viqi_storage_sparse_items', self.cached_items)
            self.h5file.set_node_attr(level_path, 'viqi_block_item_id_offsets', np.array(self.id_offsets, dtype=np.uint32))

            # store block bboxes index in the root
            BBOXS = np.zeros((len(self.bboxes), 4), np.uint32)
            for i, bbox in enumerate(self.bboxes):
                BBOXS[i, 0] = int(round(bbox[0]/power_2_level))
                BBOXS[i, 1] = int(round(bbox[1]/power_2_level))
                BBOXS[i, 2] = int(round(bbox[2]/power_2_level))
                BBOXS[i, 3] = int(round(bbox[3]/power_2_level))
            self.h5file.set_node_attr(level_path, 'viqi_storage_block_bboxes', BBOXS)

    def _store_block(self):
        if self.blocked_store is False:
            return
        if len(self.cache)<1:
            log.debug('Skipping sparse block %s due to empty cache, file: %s', self.block_id, self.filename)
            self._advance_block()
            return

        log.debug('Storing sparse block %s with %s items, file: %s', self.block_id, len(self.cache), self.filename)
        self._start_batch()
        self._append_cache_to_table()

        IDX = np.zeros((len(self.cache), 6), np.uint32)
        pos = 0
        BW = 0
        for i, img in enumerate(self.cache):
            IDX[i, 0] = self.item_ids[i]
            IDX[i, 1] = pos
            IDX[i, 2] = self.item_bbox[i][0]
            IDX[i, 3] = self.item_bbox[i][1]
            IDX[i, 4] = img.shape[0]
            IDX[i, 5] = img.shape[1]
            pos += ensure_even_size_with_padding(img.shape[0])
            BW = pos

        self.id_offsets.append(self.item_ids[0])
        img = self.cache[0]
        #BW = np.sum(IDX[:, 4]) # computed with paddings
        BH = ensure_even_size_with_padding(np.max(IDX[:, 5]))
        if len(img.shape)==3:
            BMX = np.zeros((BW, BH, img.shape[2]), img.dtype)
            chunkshape = (256, BH, BMX.shape[2])
        else:
            BMX = np.zeros((BW, BH), img.dtype)
            chunkshape = (256, BH)

        for i, img in enumerate(self.cache):
            pos = IDX[i, 1]
            if len(img.shape)==3:
                BMX[pos:pos+img.shape[0], 0:img.shape[1], :] = img
            else:
                BMX[pos:pos+img.shape[0], 0:img.shape[1]] = img

        # recompute positions and store sizes
        if self.block_bbox is None:
            # recompute here, right now we use relative coordinates in sub-elements
            pass

        path = posixpath.join(self.path, 'level_000')
        block_name = 'block_{0:04}'.format(self.block_id)
        block_path = posixpath.join(path, block_name)

        atom = tables.Atom.from_dtype(BMX.dtype)
        self.h5file.create_carray(path, block_name, obj=BMX, createparents=True, filters=self.filters, shape=BMX.shape, atom=atom, chunkshape=chunkshape)
        self.h5file.set_node_attr(block_path, 'viqi_block_bbox', np.array(self.block_bbox, dtype=np.uint32)) # extent of the transformed block in the image space
        #self.h5file.set_node_attr(block_path, 'viqi_block_transformation', []) # dima: mapping of the block into image space, skipping this defines a simple translation
        self.h5file.set_node_attr(block_path, 'viqi_block_format', 'sparse')
        self.h5file.set_node_attr(block_path, 'viqi_block_content', self.image_content)

        index_name = '{0}_sparse_index'.format(block_name)
        index_path = posixpath.join(path, index_name)
        idx_atom = tables.Atom.from_dtype(IDX.dtype)
        chunkshape = (256, IDX.shape[1])
        #self.h5file.create_array(path, index_name, obj=IDX, createparents=True, shape=IDX.shape, atom=idx_atom)
        self.h5file.create_carray(path, index_name, obj=IDX, createparents=True, filters=self.filters, shape=IDX.shape, atom=idx_atom, chunkshape=chunkshape)
        self.h5file.set_node_attr(index_path, 'viqi_index_dims', 'item_id,block_pos,x,y,w,h')

        # store pyramid
        self._store_block_pyramid(self.path, block_name, BMX, IDX)
        self._store_block_metadata(level=0, scale=1.0)

        self._advance_block()
        self._finish_batch()

    def _append_cache_to_table(self):
        if self.feature_table_defs is None:
            return
        if self.item_features is None or len(self.item_features) == 0:
            return

        try:
            table = self.h5file.get_node(self.path, self.table_name)
        except Exception:
            table = self.h5file.create_table(self.path, self.table_name, createparents=True, description=self.feature_table_defs, expectedrows=self.expected_elements, filters=self.filters)

        field_names = self.feature_table_defs.names
        row = table.row
        for i, img in enumerate(self.cache):
            features = self.item_features[i]
            if features is None:
                continue
            for k,v in features.items():
                if k in field_names:
                    row[k] = v
            row.append()
        table.flush()

    #------------------------------------------------------------------------------
    # create required indices
    #------------------------------------------------------------------------------

    def append_metadata(self, tags):
        log.debug('Append metadata %s', tags)
        self._start_batch()
        try:
            for t,v in tags.items():
                self.h5file.set_node_attr(self.path, t, v)
        except Exception:
            log.exception('Error while creating indices in %s', self.filename)
            pass
        self._finish_batch()

    #------------------------------------------------------------------------------
    # writing dense blocks
    #------------------------------------------------------------------------------

    def _store_dense_block(self, level, block_id, image, bbox):
        log.debug('_store_dense_block %s at level: %s, bbox: %s, shape: %s', block_id, level, str(bbox), str(image.shape))
        path = posixpath.join(self.path, 'level_{0:03}'.format(level))
        block_name = 'block_{0:04}'.format(block_id)
        block_path = posixpath.join(path, block_name)
        chunkshape = tuple(map(min, zip(self.chunkshape, image.shape))) #(256,256) #image.shape
        atom = tables.Atom.from_dtype(image.dtype)
        self.h5file.create_carray(path, block_name, obj=image, createparents=True, filters=self.filters, shape=image.shape, atom=atom, chunkshape=chunkshape)
        self.h5file.set_node_attr(block_path, 'viqi_block_bbox', np.array(bbox, dtype=np.uint32)) # extent of the transformed block in the image space
        #self.h5file.set_node_attr(block_path, 'viqi_block_transformation', []) # dima: mapping of the block into image space, skipping this defines a simple translation
        self.h5file.set_node_attr(block_path, 'viqi_block_format', 'dense')
        self.h5file.set_node_attr(block_path, 'viqi_block_content', self.image_content)
