"""
VQI image format support class for reading and writing dense and sparse images,objects,tables
"""

from __future__ import division

from builtins import str
from builtins import range
from builtins import object

__author__    = 'Dmitry Fedorov <dima@viqi.org>'
__version__   = '2.0'
__copyright__ = 'ViQi Inc'

import os
import sys
import traceback
import posixpath
from datetime import datetime
from collections import OrderedDict
import numpy as np
import csv
import math
import tables
#import skimage
import skimage.io
import skimage.transform

import logging
log = logging.getLogger(__name__ if __name__ != "__main__" else __file__.replace('.py', ''))


from vqi_format_reader import VQIFormatReader
from vqi_format_writer import VQIFormatWriter

from vqi_format_reader import bboxes_intersect, draw_sub_region
from vqi_format_writer import SKIMAGE_OLD_VERSION, image_pyramid_reduce, image_stats

#------------------------------------------------------------------------------
# VQIFormat - reads/writes and optimizes the VQI file
#------------------------------------------------------------------------------

class VQIFormat(VQIFormatWriter):

    #------------------------------------------------------------------------------
    # create required indices
    #------------------------------------------------------------------------------

    def create_indices(self, columns_to_index=None):
        log.debug('Creating indices for columns %s in %s', str(columns_to_index), self.filename)
        if columns_to_index is None:
            return
        self._start_batch()
        try:
            table = self.h5file.get_node(self.path, self.table_name)
            for colname in columns_to_index:
                #table.cols._f_col(colname).create_index(kind='ultralight')
                table.cols._f_col(colname).create_index(optlevel=9, kind='full')
            table.flush()
        except Exception:
            log.exception('Error while creating indices in %s', self.filename)
            pass
        self._finish_batch()

    #------------------------------------------------------------------------------
    # dense re-gridding
    #------------------------------------------------------------------------------

    def _fetch_dense_region(self, level, bbox, block_bboxes):
        log.debug('_fetch_dense_region at level: %s, bbox: %s', level, str(bbox))
        region = None
        N = block_bboxes.shape[0]
        for i in range(N):
            bbox2 = block_bboxes[i,:]
            is_intersecting = bboxes_intersect(bbox, bbox2)
            if is_intersecting == True:
                #log.debug('%s integrating %s', str(bbox), i)
                block_image = self._fetch_block(level, i, bbox=bbox2)
                #log.debug('Block image shape: %s', block_image.shape)
                if region is None:
                    self.image_dtype = block_image.dtype
                    if len(block_image.shape) == 3:
                        region = np.zeros((bbox[2],bbox[3],block_image.shape[2]), self.image_dtype)
                    else:
                        region = np.zeros((bbox[2],bbox[3]), self.image_dtype)
                offset = [0]*len(block_image.shape)
                offset[0] = bbox2[0]-bbox[0]
                offset[1] = bbox2[1]-bbox[1]
                #log.debug('offset: %s', offset)
                draw_sub_region(region, block_image, tuple(offset))

        return region

    def _regrid_next_resolution_level(self, level):
        node_level = self.h5file.get_node(self.path, 'level_{0:03}'.format(level))
        scale = node_level._v_attrs['viqi_level_scale']
        number_blocks = node_level._v_attrs['viqi_storage_number_blocks']
        #sparse_items = node_level._v_attrs['viqi_storage_sparse_items']
        #item_id_offsets = node_level._v_attrs['viqi_block_item_id_offsets']
        block_bboxes = node_level._v_attrs['viqi_storage_block_bboxes']

        # reconstruct lowest res level and store one level down
        block_id = 0
        tile_size = self.tile_size*2
        level_size = [int(round(i*scale)) for i in self.space_dims]
        log.debug('Current level: %s scale: %s, size: %s', level, scale, level_size)
        bboxes = []
        for i in range(0, level_size[0], tile_size):
            for j in range(0, level_size[1], tile_size):
                w = min(tile_size, level_size[0]-i)
                h = min(tile_size, level_size[1]-j)
                bbox = (i, j, w, h)
                block_image = self._fetch_dense_region(level, bbox, block_bboxes)
                log.debug('Processing %s, fetched %s', str(bbox), str(block_image.shape))
                #skimage.io.imsave('block_image_%s.png'%(str(bbox)), block_image)

                stats = image_stats(block_image)
                block_image = image_pyramid_reduce(block_image, preserve_type=True, stats=stats)
                bbox_lower = (int(round(i/2.0)), int(round(j/2.0)), int(round(block_image.shape[0])), int(round(block_image.shape[1])))
                #skimage.io.imsave('block_image_%s.png'%(str(bbox_lower)), block_image)
                self._store_dense_block(level+1, block_id, block_image, bbox=bbox_lower)
                block_id += 1
                bboxes.append(bbox_lower)

        # store resolution-level metadata
        path_level = posixpath.join(self.path, 'level_{0:03}'.format(level+1))

        self.h5file.set_node_attr(path_level, 'viqi_level_scale', scale/2.0)
        self.h5file.set_node_attr(path_level, 'viqi_storage_number_blocks', block_id)

        # store block bboxes index in the root
        BBOXS = np.zeros((len(bboxes), 4), np.uint32)
        for i, bbox in enumerate(bboxes):
            BBOXS[i, :] = bbox
        self.h5file.set_node_attr(path_level, 'viqi_storage_block_bboxes', BBOXS)

    def regrid_lower_resolution_levels(self):
        log.debug('Creating dense grid for low-res levels, file: %s', self.filename)
        self._start_batch()
        self.space_dims = self.h5file.get_node_attr(self.path, 'viqi_image_size')

        if self.image_dtype is None:
            self.image_dtype = np.uint8 # dima: we need to find this from blocks stored in the image

        log.debug('self.space_dims %s', self.space_dims)
        log.debug('self.image_dtype %s', self.image_dtype)

        # find lowest available resolution and re-grid all lower resolution levels from there
        level = 0
        while level<100:
            level_path = posixpath.join(self.path, 'level_{0:03}'.format(level))
            if self.h5file.__contains__(level_path) is False:
                level -= 1
                break
            level += 1

        log.debug('level %s', level)

        if level<0:
            return

        scale = self.h5file.get_node_attr(posixpath.join(self.path, 'level_{0:03}'.format(level)), 'viqi_level_scale')
        while scale>0.0000000000000001:
            level_size = [int(round(i*scale)) for i in self.space_dims]
            if level_size[0]<self.tile_size and level_size[1]<self.tile_size:
                break
            log.debug('\nRe-gridding level: %s, file: %s', level+1, self.filename)

            self._regrid_next_resolution_level(level)
            level += 1
            scale = scale/2.0

        self._finish_batch()

    #------------------------------------------------------------------------------
    # re-coloring
    #------------------------------------------------------------------------------

    def recolor_by_column(self, column_id, column_value, minv=0, maxv=100):
        log.debug('Creating coloring for all levels, file: %s', self.filename)
        self._start_batch()
        self.space_dims = self.h5file.get_node_attr(self.path, 'viqi_image_size')

        if self.image_dtype is None:
            self.image_dtype = np.uint8 # dima: we need to find this from blocks stored in the image

        # read object_id and column to an array for recoloring
        colors = {}
        rng = 235.0/(maxv-minv)
        self.objects = {}
        table = self.h5file.get_node(self.path, 'table')
        for row in table.iterrows():
            idx = row[column_id]
            v = row[column_value]
            vv = 20 + int((v - minv) * rng)
            self.objects[idx] = vv
            colors[int(v)] = vv
        #log.debug('Colors: %s', colors)

        # find lowest available resolution and re-grid all lower resolution levels from there
        level = 0
        while level<100:
            level_path = posixpath.join(self.path, 'level_{0:03}'.format(level))
            if self.h5file.__contains__(level_path) is False:
                break
            self._recolor_resolution_level(level)
            level += 1

        self._finish_batch()
        return colors

    def _recolor_resolution_level(self, level):
        try:
            sparse_items = self.h5file.get_node_attr(posixpath.join(self.path, 'level_{0:03}'.format(level)), 'viqi_storage_sparse_items')
        except Exception:
            sparse_items = 0

        if sparse_items>0:
            self._recolor_resolution_level_sparse(level)
        else:
            self._recolor_resolution_level_dense(level)

    def _recolor_resolution_level_sparse(self, level):
        node_level = self.h5file.get_node(self.path, 'level_{0:03}'.format(level))
        number_blocks = node_level._v_attrs['viqi_storage_number_blocks']

        for block_id in range(number_blocks):
            path = posixpath.join(self.path, 'level_{0:03}'.format(level))
            block_name = 'block_{0:04}'.format(block_id)
            block_path = posixpath.join(path, block_name)

            # sparse block may not be present if there were no elements in it
            try:
                node_block = self.h5file.get_node(path, block_name)
                self._recolor_sparse_block(node_block, path, block_name)
            except Exception:
                log.exception('Exception while re-coloring block: %s', block_name)

    def _recolor_sparse_block(self, node, path, block_name):
        page = node.read()
        index_name = '{0}_sparse_index'.format(block_name)
        node_index = self.h5file.get_node(path, index_name)
        page_index = node_index.read()
        N = page_index.shape[0]
        for i in range(N):
            idx = page_index[i, 0]
            pos = page_index[i, 1]
            x = page_index[i, 2]
            y = page_index[i, 3]
            w = page_index[i, 4]
            h = page_index[i, 5]
            val = self.objects[idx]
            try:
                img = page[pos:pos+w,0:h]
                img[img>0] = val
                page[pos:pos+w,0:h] = img
            except Exception:
                log.exception('Exception while re-coloring block: %s, id: %s', block_name, idx)
        # write page back
        node[:,:] = page

    def _recolor_resolution_level_dense(self, level):
        if level<1:
            return
        node_level = self.h5file.get_node(self.path, 'level_{0:03}'.format(level))
        number_blocks = node_level._v_attrs['viqi_storage_number_blocks']
        block_bboxes = node_level._v_attrs['viqi_storage_block_bboxes']
        scale = node_level._v_attrs['viqi_level_scale']

        level2 = level-1
        node_level2 = self.h5file.get_node(self.path, 'level_{0:03}'.format(level2))
        number_blocks2 = node_level2._v_attrs['viqi_storage_number_blocks']
        block_bboxes2 = node_level2._v_attrs['viqi_storage_block_bboxes']
        scale2 = node_level2._v_attrs['viqi_level_scale']

        for block_id in range(number_blocks):
            path = posixpath.join(self.path, 'level_{0:03}'.format(level))
            block_name = 'block_{0:04}'.format(block_id)
            block_path = posixpath.join(path, block_name)
            bbox = block_bboxes[block_id, :]

            # sparse block may not be present if there were no elements in it
            try:
                node_block = self.h5file.get_node(path, block_name)
                i = int((bbox[0]/scale)*scale2)
                j = int((bbox[1]/scale)*scale2)
                w = int((bbox[2]/scale)*scale2)
                h = int((bbox[3]/scale)*scale2)
                bbox2 = (i,j,w,h)
                #log.debug('Dense block bbox: %s from upper scale %s', bbox, bbox2)
                block_image = self._fetch_dense_region(level2, bbox2, block_bboxes2)
                #log.debug('Processing %s, fetched %s', str(bbox2), str(block_image.shape))
                #skimage.io.imsave('block_image_%s.png'%(str(bbox)), block_image)

                stats = image_stats(block_image)
                block_image = image_pyramid_reduce(block_image, preserve_type=True, stats=stats)

                node_block[:,:] = block_image
            except Exception:
                log.exception('Exception while re-coloring dense block: %s', block_name)

    #------------------------------------------------------------------------------
    # matching with manual annotations
    #------------------------------------------------------------------------------

    def set_classes_from_annotations(self, gt, class_map, max_distance):
        '''
        gt - are a list of gobjects
        class_map - a dictionary with a string label as a key mapping to numerical label
        max_distance - a maximum distance in pixels to accept a match
        '''
        log.debug('Updating class labels from manual annotations, file: %s', self.filename)

        self._start_batch(mode='a')
        node_level = self.h5file.get_node(self.path, 'level_{0:03}'.format(level))
        number_blocks = node_level._v_attrs['viqi_storage_number_blocks']

        # walk block by block
        for block_id in range(number_blocks):
            # read objects
            objs = self.read_block_items(block_id, level=level)
            # compute centroids in whole image coordinates
            for o in objs:
                block_bbox = o['_block_bbox']
                bbox = o['_bbox']
                i = bbox[0] + bbox[2]/2.0 + block_bbox[0]
                j = bbox[1] + bbox[3]/2.0 + block_bbox[1]
                o['centroid'] = (i,j)
            # match this block
            matched = self._annotate_classes_from_GT(objs, gt, class_map, max_distance)
            self._update_table_rows(matched)
        self._finish_batch()

    def _annotate_classes_from_GT(objs, gt, class_map, max_distance):
        '''
        use the simplest matching iterating through gt, since we expect fewer points manually annotated
        '''
        log.debug('Ground truth count: %s, with %s objects, using max distance: %s', len(gt), len(objs), max_distance)
        col_label = self.column_label
        matched = []
        #matched_gt = [] will not reduce the list sizes, they are very small for now
        for g in gt:
            closest = None
            closest_d = 1234567890
            label = g.type
            label_num = class_map[g.type]

            for o in objs:
                # use euclidean distance
                x1 = g.vertices[0][0]
                y1 = g.vertices[0][1]
                x2 = o['centroid'][0]
                y2 = o['centroid'][1]
                d = math.sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) )
                if d<=max_distance and d<closest_d:
                    closest = o
                    closest_d = d
            if closest is not None:
                closest[col_label] = label_num
                matched.append(closest)
        return matched

    def _update_table_classes(self, objs):
        col_label = self.column_label
        col_id = self.column_id
        try:
            table = self.h5file.get_node(self.path, 'table')
            condvars = {'label_col': table.colinstances[col_label]}
            for o in obj:
                query = '(%s==%s)'%(col_id, o[col_id])
                r = table.read_where(query, condvars=condvars)
                if len(r)<1: continue
                r[0][col_label] = o[col_label]
                r[0].update()
            table.flush()
        except Exception:
            log.exception('Error while updating class label')

