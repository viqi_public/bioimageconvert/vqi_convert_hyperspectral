import sys
import os
import libtiff
import math
import numpy as np
import ctypes

import skimage.io

from vqi_format import VQIFormat

import logging
logging.basicConfig(level=logging.DEBUG)



filename = sys.argv[1]
filename2 = sys.argv[2]

tiff = libtiff.TIFF.open(filename, mode='r')
#img = tiff.read_image()


width = tiff.GetField('ImageWidth')
height = tiff.GetField('ImageLength')
bits = tiff.GetField('BitsPerSample')
itemsize = int(bits / 8)
samples = tiff.GetField('SamplesPerPixel')
RS = tiff.GetField('RowsPerStrip')
sample_format = tiff.GetField('SampleFormat')
compression = tiff.GetField('Compression')
typ = tiff.get_numpy_type(bits, sample_format)

print('({}x{}x{}) Rows/Sampl: {}, type: {}'.format(width, height, samples, RS, typ))


chunk_width = width
chunk_height = 256
chunk_size = chunk_width * chunk_height * samples * itemsize
strip_size = chunk_width * samples * itemsize
chunk_number = int(math.ceil(height*1.0 / chunk_height))

vqi_writer = VQIFormat(
    filename2,
    title='Spectrometer',
    path='/spectra',
    space_dims=(height, width),
    #feature_table_defs=self.get_column_types(self.table_definition),
    expected_elements=3000000,
    #column_id='cell_id',
    #column_ref='mask',
    image_content='spectral',
    chunkshape=(256,256,1),
    storage_type='image',
    image_dimensions='Y,X,C',
    compression_level=1,
    tile_size=256)


# read/write
strip = 0
block_y = 0
for chunk in range(chunk_number):
    print('Reading strips ', end = '')
    p = 0
    chunk_height = min(chunk_height, height-block_y)
    arr = np.zeros((chunk_height, chunk_width, samples), typ)
    for pos in range(chunk_height):
        if strip>=height: break
        elem = tiff.ReadEncodedStrip(strip, arr.ctypes.data + p, strip_size)
        print('%s.'%strip, end = '', flush=True)
        strip += 1
        p += strip_size

    # write blocks 9685 x 256 x 324, chunked by 256x256x1
    print('\nWriting chunk: %s at: %s with strips: %s'%(chunk, block_y, chunk_height))

    # print('min: %s, max: %s'%(arr.min(), arr.max()))
    # np.clip(arr, 0, None, out=arr) # spectral values should be above 0
    # img = arr[:,:,200]
    # img = np.squeeze(img)
    # print('image shape: %s'%(str(img.shape)))
    # skimage.io.imsave('test_%s.tif'%(chunk), img)
    # if chunk > 2: break

    np.clip(arr, 0, None, out=arr) # spectral values should be above 0
    bbox = (block_y, 0, chunk_height, chunk_width)
    vqi_writer.write_dense_block(chunk, arr, bbox=bbox, level=0, scale=1.0)
    block_y += chunk_height

tiff.close()

wavelengths = np.array(list(range(380, 2540, 5)), dtype=np.uint32)
tags = {
     'viqi_spectral_wavelengths': wavelengths,
     'viqi_spectral_wavelengths_units': 'nm',
}
vqi_writer.append_metadata(tags)
vqi_writer.regrid_lower_resolution_levels()
